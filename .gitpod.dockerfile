FROM ubuntu:20.04

ENV DEBIAN_FRONTEND=noninteractive
ENV workspace=/wksp
# software-properties-common python3-pip should be installed if you want python3.9
RUN apt-get update && apt-get install -y curl file vim git xz-utils unzip tzdata software-properties-common python3-pip \
    &&  echo "export LC_ALL=C.UTF-8" >> ~/.bashrc && ln -fs /usr/share/zoneinfo/Asia/Taipei /etc/localtime && dpkg-reconfigure -f noninteractive tzdata \
    && apt-get clean && rm -rf /var/cache/apt/* && rm -rf /var/lib/apt/lists/* && rm -rf /tmp/*

# install python3.9 and upgrade the related pip
RUN add-apt-repository ppa:deadsnakes/ppa -y \
    && apt-get update && apt-get install -y python3.9 \
    && python3.9 -m pip install --upgrade pip \
    && ln -fs /usr/bin/python3.9 /usr/bin/python3 \
    && apt-get clean && rm -rf /var/cache/apt/* && rm -rf /var/lib/apt/lists/* && rm -rf /tmp/*
RUN mkdir $workspace
WORKDIR $workspace
ARG GO_VERSION="go1.15.3.linux-amd64"
ARG FFMPEG_VERSION="ffmpeg-4.2.2-amd64-static"
RUN mkdir -p /wksp/go && curl -O -sSL https://dl.google.com/go/${GO_VERSION}.tar.gz \
    && tar -xf ${GO_VERSION}.tar.gz && rm ${GO_VERSION}.tar.gz \
    && echo "export GOROOT=$workspace/go" >> ~/.bashrc \
    && echo "export PATH=$PATH:$workspace/go/bin" >> ~/.bashrc 

RUN mkdir -p /wksp/avs && cd /wksp/avs && curl -o ffm.xz -sSL https://www.johnvansickle.com/ffmpeg/old-releases/${FFMPEG_VERSION}.tar.xz \
    && tar -xf ffm.xz \
    && mkdir -p ffmpeg/linux \
    && cp ${FFMPEG_VERSION}/ffmpeg ffmpeg/linux \
    && rm -rf ffm.xz ${FFMPEG_VERSION} \
    && cp /wksp/avs/ffmpeg/linux/ffmpeg /usr/bin/


CMD ["/bin/bash"]